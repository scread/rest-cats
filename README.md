# RestCats
RestCats is a project for studying back-end features

## Core features:
App base url: `localhost:8080/api`
* GET `/cats` expected response list of cats or empty list
* POST `/cats` add new cat. Expected JSON cat
Example request
Cat's ID is provided by app
```
{
  "name": "cat",
  "age": 1
}
```
Response example
```
{
  "id": 4,
  "name": "cat",
  "age": 1
}
```
* PUT `/cats/{id}` update cat's params
Example request
```
{
  "name": "new_cat",
  "age": 2
}
```
* DELETE `/cats/{id}` delete cat with given ID

## Filters:
* Server filters
  - Container request filter
  - Container response filter
* Client filters
  - Client request filter
  - Client response filter

## How to run the project:
1. Check-out the project
2. run `mvn jetty:run` in root folder