package service.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class PetsExceptionHandler implements ExceptionMapper<PetsException> {

    public Response toResponse(PetsException exception) {
        ErrorMessageEntity message = new ErrorMessageEntity();
        message.setMessage(exception.getMessage());

        return Response.status(exception.getStatusCode())
                .entity(message)
                .build();
    }
}