package service.exceptions;

import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class ServerExceptionHandler implements ExceptionMapper<ServerErrorException> {

    public Response toResponse(ServerErrorException exception) {
        ErrorMessageEntity message = new ErrorMessageEntity();
        message.setMessage(exception.getCause().toString());

        return Response.status(exception.getResponse().getStatusInfo())
                .type(MediaType.APPLICATION_JSON)
                .entity(message)
                .build();
    }
}
