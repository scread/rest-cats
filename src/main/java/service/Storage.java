package service;

import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;

@Service
public class Storage {

    private Map<Integer, Cat> catStorage = new HashMap<>();
    private int idCounter = 0;

    @PostConstruct
    public void init() {
        putCat(new Cat("Tom", 3));
        putCat(new Cat("Pushok", 5));
        putCat(new Cat("Murzic", 2));
    }

    public int putCat(Cat cat) {
        cat.setId(++idCounter);
        catStorage.put(cat.getId(), cat);
        return cat.getId();
    }

    public Cat getCatByID(int id) {
        return catStorage.get(id);
    }

    public boolean deleteCat(int id) {
        Cat removeCat = catStorage.remove(id);
        return removeCat != null;
    }

    public boolean editCat(int id, Cat cat) {
        if (catStorage.containsKey(id)) {
            Cat storageCat = catStorage.get(id);
            int newAge = cat.getAge();
            String newName = cat.getName();
            if (cat.getAge() != 0) {
                storageCat.setAge(newAge);
            }
            if (cat.getName() != null) {
                storageCat.setName(newName);
            }
            return true;
        }
        return false;
    }

    public List<Cat> getAllCats() {
        List<Cat> catList = new ArrayList<Cat>();
        Set<Integer> keySet = catStorage.keySet();
        for (int key : keySet) {
            catList.add(catStorage.get(key));
        }
        return catList;
    }

}
