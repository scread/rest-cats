package service;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;

public class CatDeserializer extends JsonDeserializer<Cat> {

    @Override
    public Cat deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        String name = null;
        Integer age = null;

        JsonNode node = jsonParser.getCodec().readTree(jsonParser);

        JsonNode id = node.get("id");
        if (id != null) {
            throw new IOException("Cat's ID cant be provided");
        }

        JsonNode nameNode = node.get("name");
        if (nameNode != null && !"".equals(nameNode.asText())) {
            name = nameNode.asText();
        } else {
            throw new IOException("Name cant be null");
        }

        JsonNode ageNode = node.get("age");
        if (ageNode != null && ageNode.asInt() >= 0) {
            age = ageNode.asInt();
        } else {
            throw new IOException("Age cant be null or less zen zero");
        }

        return new Cat(name, age);
    }
}
