package service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import service.exceptions.PetsError;
import service.exceptions.PetsException;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Service
@Path("/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class RestService {

    private Storage storage;

    @Autowired
    public RestService(Storage storage) {
        this.storage = storage;
    }

    @GET
    @Path("/cats")
    public List<Cat> getAllCats() {
        return storage.getAllCats();
    }

    @GET
    @Path("/cats/{id}")
    public Cat getCat(@PathParam("id") int id) {
        Cat cat = storage.getCatByID(id);
        if (cat != null) {
            return cat;
        } else {
            throw new PetsException(PetsError.E0001_CAT_NOT_FOUND);
        }
    }

    @POST //add
    @Path("/cats")
    public Cat addCat(@Valid Cat cat) {
        int newCatId = storage.putCat(cat);
        return getCat(newCatId);
    }

    @PUT //edit
    @Path("/cats/{id}")
    public Cat editCat(@PathParam("id") int id, @Valid Cat cat) {
        if (id <= 0) {
            throw new PetsException(PetsError.E0002_WRONG_CAT);
        }
        boolean catIsExist = storage.editCat(id, cat);
        if (catIsExist) {
            return getCat(id);
        } else {
            throw new PetsException(PetsError.E0001_CAT_NOT_FOUND);
        }
    }

    @DELETE
    @Path("/cats/{id}")
    public Response deleteCat(@PathParam("id") int id) {
        if (storage.deleteCat(id)) {
            Response.Status status = Response.Status.NO_CONTENT;
            return Response.status(status).build();
        } else {
            throw new PetsException(PetsError.E0002_WRONG_CAT);
        }
    }
}
