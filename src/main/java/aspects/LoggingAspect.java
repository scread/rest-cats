package aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.RestService;

import javax.ws.rs.ServerErrorException;

@Aspect
public class LoggingAspect {

    private static final Logger LOG = LoggerFactory.getLogger(RestService.class);

    @Before("execution(* service.RestService.*(..))")
    public void logBefore(JoinPoint joinPoint) {
        StringBuilder sb = new StringBuilder();
        sb.append(joinPoint.getSignature().getName());
        Object[] objects = joinPoint.getArgs();
        for (Object arg: objects) {
            sb.append(", ").append(arg.toString());
        }
        LOG.info(sb.toString());
    }

    @Before("execution(* service.exceptions.ServerExceptionHandler.*(..))")
    public void logServerExceptionHandler(JoinPoint joinPoint) {
        LOG.error("Server error", (ServerErrorException) joinPoint.getArgs()[0]);
    }
}
