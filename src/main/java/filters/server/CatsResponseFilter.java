package filters.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

@PreMatching
public class CatsResponseFilter implements ContainerRequestFilter {
    private static final Logger logger = LoggerFactory.getLogger(CatsResponseFilter.class);

    @Override
    public void filter(ContainerRequestContext containerRequestContext) throws IOException {
        final URI absolutePath = containerRequestContext.getUriInfo().getAbsolutePath();
        String path = absolutePath.getPath();

        if (path.contains("/getAllCats")) {
            path = path.replace("/getAllCats", "/cats");
        }

        try {
            containerRequestContext.setRequestUri(new URI(path));
        } catch (URISyntaxException e) {
            logger.error("URI replace exception", e);
        }
    }
}
